import country_converter as coco

def add_ref_label(data,add_ref_area_label):
    
    data.insert(1, 'ref_area.label' ,'')
    
    for ref in add_ref_area_label['ref_area'].unique():
        data.loc[data['ref_area']==ref,['ref_area.label']]=add_ref_area_label.loc[add_ref_area_label['ref_area']==ref,['ref_area.label']]    
    
    
    '''
    In ILO table, the ISO3 associated to channel island is CHA. However, in coco CHI is allocated to this location.
    We replace CHA by CHI in ILO table
    '''

    
    data["ref_area"].replace({"CHA": "CHI"}, inplace=True)
   
    '''
    We add the EXIO3 region for each ISO3 countries 
    '''
    
    country_code = list(data['ref_area'])
    cc_all = coco.CountryConverter(include_obsolete=True)
    data.insert(2, 'EXIO3', cc_all.convert(names = country_code,src="ISO3", to='EXIO3'))

    return data